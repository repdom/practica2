<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>${titulo}</title>
</head>
<body>
    <#include "/navbar.ftl">
    <h1>Estudiante Agregado</h1>
    <table>
        <tbody>
            <tr><td>Matricula: </td><td>${estudiante.matricula}</td></tr>
            <tr><td>Nombre: </td><td>${estudiante.nombre}</td></tr>
            <tr><td>Apellido: </td><td>${estudiante.apellido}</td></tr>
        </tbody>
    </table>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>