<!doctype html>
<html lang="en">
<title>${titulo}</title>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>Estudiante Ingresado</title>
</head>
<body>
    <#include "/navbar.ftl">
    <div class="row">
        <form action="/ingresarNuevoEstudiante/" method="post" class="col s12">
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="Nombre" name="nombre" type="text" class="validate">
                </div>
                <div class="input-field col s6">
                    <input placeholder="Apellido" name="apellido" type="text" class="validate">
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="Matricula" min="0" name="matricula" type="number" class="validate">
                </div>
                <div class="input-field col s6">
                    <input placeholder="Telefono" name="telefono" type="text" class="validate">
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <button class="btn waves-effect waves-light col s12" type="submit" name="Enviar">Enviar
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>