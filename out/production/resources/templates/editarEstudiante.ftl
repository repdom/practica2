<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<#include "/navbar.ftl">
    <form action="/datosEstudiante/${estudiante.matricula}" method="post" class="col s12">
        <div class="row">
            <div class="input-field col s6">
                <input placeholder="Nombre" name="nombre" type="text" class="validate" value="${estudiante.nombre}">
            </div>
            <div class="input-field col s6">
                <input placeholder="Apellido" name="apellido" type="text" class="validate" value="${estudiante.apellido}">
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6">
                <input placeholder="Matricula" min="0" disabled name="matricula" type="number" class="validate" value="${estudiante.matricula}">
            </div>
            <div class="input-field col s6">
                <input placeholder="Telefono" name="telefono" type="text" class="validate" value="${estudiante.telefono}">
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <!--<a href="/datosEstudiante/${estudiante.matricula}/${estudiante.nombre}/${estudiante.apellido}/${estudiante.telefono}">-->
                    <button class="btn waves-effect waves-light col s12" type="submit" name="Enviar">Actualizar
                        <i class="material-icons right">send</i>
                    </button>
                <!--</a>-->
            </div>
        </div>
    </form>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

</body>
</html>