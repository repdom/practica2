package com.practica2.pucmm.clases;

public class Estudiante {
    private int matricula;
    private String nombre;
    private String apellido;
    private String telefono;

    /**
     *
     * @param matricula
     * @param nombre
     * @param apellido
     * @param telefono
     */

    public Estudiante(int matricula, String nombre, String apellido, String telefono) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.telefono = telefono;
        this.matricula = matricula;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
