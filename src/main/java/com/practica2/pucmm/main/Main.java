package com.practica2.pucmm.main;

import com.practica2.pucmm.clases.Estudiante;
import freemarker.template.Configuration;
import spark.ModelAndView;
import spark.Redirect;
import spark.Spark;
import spark.template.freemarker.FreeMarkerEngine;

import java.util.*;

import static spark.route.HttpMethod.get;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class Main {
    static ArrayList<Estudiante> estudiantes = new ArrayList<Estudiante>();

    public static void main(String[] args) {
        Configuration configuration=new Configuration(Configuration.getVersion());
        configuration.setClassForTemplateLoading(Main.class, "/templates");
        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine(configuration);

        estudiantes.add(new Estudiante(25, "Juan T.", "Ángel Montilla", "849-639-4576"));
        estudiantes.add(new Estudiante(35, "Juanillo", "Juani", "849-659-4076"));

        Spark.get("/formulario/", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Ingresar Estudiante");
            return new ModelAndView(attributes, "formulario.ftl");
        }, freeMarkerEngine);
        Spark.post("/ingresarNuevoEstudiante/", (request, response) -> {
            int matricula = Integer.parseInt(request.queryParams("matricula"));
            String nombre = request.queryParams("nombre");
            String apellido = request.queryParams("apellido");
            String telefono = request.queryParams("telefono");

            Estudiante estudiante = new Estudiante(matricula, nombre, apellido, telefono);
            // estudiantes.add(estudiante); + estudiantes.size()
            estudiantes.add(estudiante);
            System.out.println("Nombre Individuo: "+ nombre + " ");
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Estudiante Agregado");
            attributes.put("estudiante", estudiante);

            return new ModelAndView(attributes, "estudianteAgregado.ftl");
        }, freeMarkerEngine);
        Spark.get("/listaDeEstudiantes/", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Lista de Estudiantes");
            attributes.put("estudiantes", estudiantes);

            return new ModelAndView(attributes, "listaDeEstudiantes.ftl");
        }, freeMarkerEngine);
        Spark.get("/datosEstudiante", (request, response) -> {
            Estudiante datosEstudiante = new Estudiante(23, "", "", "");
            Map<String, Object> attributes = new HashMap<>();

            for (Estudiante es: estudiantes) {
                if(es.getMatricula() == Integer.parseInt(request.queryParams("matricula"))) {
                    datosEstudiante = es;
                }
            }
            attributes.put("titulo", "Estudiante Agregado");
            attributes.put("estudiante", datosEstudiante);

            return new ModelAndView(attributes, "editarEstudiante.ftl");
        }, freeMarkerEngine);
        Spark.post("/datosEstudiante/:matricula", (request, response) -> {
            int matricula = Integer.parseInt(request.params("matricula"));
            String nombre = request.queryParams("nombre");
            String apellido = request.queryParams("apellido");
            String telefono = request.queryParams("telefono");
            Estudiante estudiante = new Estudiante(matricula, nombre, apellido, telefono);
            System.out.println(estudiante.getMatricula());
            int i = 0;
            for (Estudiante es: estudiantes) {
                if(es.getMatricula() == matricula) {
                    estudiantes.set(i, estudiante);
                    break;
                }
                i+=1;
            }
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Lista de Estudiantes");
            attributes.put("estudiantes", estudiantes);

            response.redirect("/listaDeEstudiantes/");
            return null;
        }, freeMarkerEngine);
        Spark.get("/eliminar/:matricula", (request, response) -> {
            int matricula = Integer.parseInt(request.params("matricula"));
            for (Estudiante es: estudiantes) {
                if(es.getMatricula() == matricula) {
                    estudiantes.remove(es);
                    break;
                }
            }
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Lista de Estudiantes");
            attributes.put("estudiantes", estudiantes);

            response.redirect("/listaDeEstudiantes/");
            return "";
        });
    }
}