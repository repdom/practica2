<nav>
    <div class="nav-wrapper">
        <a href="#" class="brand-logo right">Cornelia Estudiantes</a>
        <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="/formulario/">Agregar Estudiante</a></li>
            <li><a href="/listaDeEstudiantes/">Listar Estudiante</a></li>
        </ul>
    </div>
</nav>
