<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>${titulo}</title>
</head>
<body>
    <#include "/navbar.ftl">
    <table>
        <tbody>
        <tr>
            <th>Matricula</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Telefono</th>
            <th>Acciones</th>
        </tr>
        <#list estudiantes as estudiante>
            <tr>
                <td>${estudiante.matricula?string["0"]}</td>
                <td>${estudiante.nombre}</td>
                <td>${estudiante.apellido}</td>
                <td>${estudiante.telefono}</td>
                <td>
                    <div class="btn waves-effect waves-light">
                        <a href="/datosEstudiante?matricula=${estudiante.matricula}"><i class="material-icons">edit</i></a>
                    </div>
                    <div class="btn waves-effect waves-light">
                        <a href="/eliminar/${estudiante.matricula}"><i class="material-icons">delete</i></a>
                    </div>
                </td>
            </tr>
        </#list>
        </tbody>
    </table>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>